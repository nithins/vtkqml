# VtkQml #

A simple demo on how to use qml UI over a VTK rendering.
Its based off an example of  [OpenGL under QML](http://doc.qt.io/qt-5/qtquick-scenegraph-openglunderqml-example.html), along with an example of [ExternalVTKWidget](http://www.kitware.com/blog/home/post/688).

